import re

def advanced_split(self, string, length=2000, re_group='(.)'):
    strings = []
    string_buffer = ''
    match = re.findall(re_group, string, flags=re.DOTALL)

    for seq in match:
        if len(string_buffer + seq) <= length:
            string_buffer += seq
        else:
            strings.append(string_buffer)
            string_buffer = seq

    return strings
