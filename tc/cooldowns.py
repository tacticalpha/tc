import time

import discord

class CooldownTimer:
    def __init__(self, length):
        self.end_time = time.time() + length
        self.length = length

    def __bool__(self):
        return self.done

    @property
    def done(self):
        return time.time() >= self.end_time

    def reset(self):
        self.end_time = time.time() + self.length

class UserCooldown:
    def __init__(self, duration):
        self.duration = duration
        self.users = {}

    def add(self, user : discord.User):
        self.users[user] = CooldownTimer(self.duration)

    def __getitem__(self, user : discord.User):
        if key not in self.users:
            self.add(key)
        return self.users[user]


class GuildCooldown:
    def __init__(self, duration):
        self.duration = duration
        self.guilds = {}

    def add(self, guild : discord.Guild):
        self.guilds[guild] = CooldownTimer(self.duration)

    def __getitem__(self, guild : discord.Guild):
        if guild not in self.guilds:
            self.add(key)
        return self.users[user]


class GlobalCooldown(CooldownTimer):
    """
    Just for naming consistancy
    """
    pass
