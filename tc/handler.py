import discord

import collections
import importlib
import traceback
import asyncio
import inspect
import sys
import re

from tc.exceptions import *
import tc.utils

from tc.classes import *

def split_message(text):
    parts = []
    defbuff = {
        'type': 'plain',
        'content': []
    }
    buff = dict(defbuff)
    splitting = 0
    # -1 is starting, 1 is in a part, 2 is for ending, and 0 is nothing
    split_pos = 0
    current_split = ''
    escaping = False
    split_strings = [
        '_',
        '__'
        '~~',
        '*',
        '**',
        '***',
        '`',
        '```',
        '||'
    ]
    split_lengths = []
    for i in range(len(max(split_strings))):
        temp = []
        for s in split_strings:
            if len(s) >= i+1:
                temp.append(s[i])
            else:
                temp.append(None)
        split_lengths.append(temp)
    for char in text:
        if not escaping:
            if splitting == -1:
                if char in split_lengths[split_pos]:
                    split_pos += 1
                    continue
                else:
                    split_pos = 0
                    splitting = 1
                    current_split = ''.join(buff[-split_pos:])
            elif splitting == 0:
                if char in split_lengths[0]:
                    splitting = -1
                    split_pos += 1
                    buff['type'] = 'plain'
                    parts.append(buff)
                    buff = dict(defbuff)
            elif splitting == 1:
                if char == current_split[0]:
                    splitting = 2
                    split_pos += 1
            elif splitting == 2:
                if char == current_split[split_pos]:
                    split_pos += 1
                    continue
                else:
                    if len(current_split) == (split_pos + 1):
                        buff['type'] = current_split
                        parts.append(buff)
                        buff = defbuff
                        continue
                    else:
                        splitting = 1
        buff['content'].append(char)
    parts.append(buff)
    return parts


class Handler:

    def __init__(
        self, client,
        *global_prefixes,
        default_prefixes = [],
        guild_prefixes = {},
        admins = [],
        id = None,
        context_hook=lambda ctx: None,
        auto_send_check_errors = True
    ):

        self.client           = client
        self.global_prefixes  = global_prefixes
        self.default_prefixes = default_prefixes
        self.guild_prefixes   = guild_prefixes

        self.loaded_extensions = {}
        self.global_checks     = []
        self.commands          = {}
        self.aliases           = {}
        self.events            = {}

        self.context_hook      = context_hook

        self.shared            = None

        self._ready            = False

        self.admins                   = admins
        self.ignore_checks_for_admins = False

        self.auto_send_check_errors = auto_send_check_errors


    @property
    def ready(self):
        return self._ready


    @ready.setter
    def ready(self, new_value):
        if type(new_value) == bool:
            self._ready = new_value
        else:
            raise TypeError('Ready value must have type "bool" (True/False)')

    async def create_context(self, message=None, **kwargs):
        ctx = Context(self.client, self, message, None, self.context_hook, **kwargs)
        await ctx.run_hook()

        return ctx

    def load_extension(self, file_name):
        if file_name in sys.modules:
            del sys.modules[file_name]

        extension = importlib.import_module(file_name)

        if not hasattr(extension, 'setup'):
            del extension
            raise ExtensionLoadError('Missing setup function')

        extension.setup(self)

        self.loaded_extensions[file_name] = extension


    def load_class(self, ext_class):
        for cmd in filter(lambda attr: isinstance(getattr(ext_class, attr), Command), dir(ext_class)):
            cmd = getattr(ext_class, cmd)

            if not cmd.cmd_group:
                if hasattr(ext_class, '__name__'):
                    cmd.cmd_group = ext_class.__name__
                else:
                    cmd.cmd_group = ext_class.__module__.split('.')[-1:][0]

            self.commands[cmd.name] = cmd

            for alias in cmd.aliases:
                self.aliases[alias] = cmd.name


    def unload_extension(self, name):
        pass


    def _compile_prefixes(self, *prefixes):
        """ Depreciated, unnessecary """

        processed_prefixes = []
        guild_prefixes = {}

        def compile(prefix):
            prefix_type = None
            value       = None
            groups      = None

            if type(prefix) in (tuple, list):

                if len(prefix) != 2:
                    raise TypeError("Prefix shorthand has an invalid length, {}, shorthand only accepts two basic paramaters, type and text".format(len(prefix)))

                if prefix[0] in ('exact', 'regex'):
                    prefix_type = prefix[0]
                else:
                    raise TypeError('Prefix has an invalid type (can be exact, regex), "{}"'.format(prefix[0]))

                if prefix[1] is not None:
                    value = prefix[1]
                else:
                    TypeError('Prefix must have a non-none text value')

                groups = ['*']

            elif isinstance(prefix, Prefix):

                return prefix

            else:
                raise TypeError()             ### COMPLETE THIS ###

            if prefix_type == 'exact':
                pt = ExactPrefix
            elif prefix_type == 'regex':
                pt = RegexPrefix
            else:
                raise TypeError('Invalid prefix type')

            return pt(value, groups=groups)


        for prefix in prefixes:

            prefix = compile(prefix)

            processed_prefixes.append(prefix)

        return processed_prefixes


    def add_prefix(prefix):
        self.command_prefixes.append(prefix)


    def add_guild_prefix(self, gid, prefix):
        if gid not in self.guild_prefixes:
            self.guild_prefixes[gid] = []

        self.guild_prefixes[gid].append(prefix)


    def add_check(group, function):
        if group not in self.global_checks:
            self.global_checks[group] = []

        self.global_checks[group].append(function)


    def match_prefix(self, message):
        content = message.content

        for prefix in self.global_prefixes:
            res = prefix.check(content)
            if res[0]:
                return (*res, prefix)

        gid = message.guild.id
        if gid in self.guild_prefixes and len(self.guild_prefixes[gid]) != 0:
            prefixes = self.guild_prefixes[gid]
        else:
            prefixes = self.default_prefixes

        for prefix in prefixes:
            res = prefix.check(content)
            if res[0]:
                return (*res, prefix)

        return (False, '', None)

    def parse_args_v1(self, ctx, msg):
        args = []
        in_string = False
        string_buffer = []

        for arg in msg:
            if arg.startswith('"') and not in_string:
                in_string = True
                string_buffer.append(arg[1:])

            elif arg.endswith('"') and in_string:
                in_string = False
                string_buffer.append(arg[:-1])
                args.append(' '.join(string_buffer))
                string_buffer = []

            elif in_string:
                string_buffer.append(arg)

            else:
                args.append(arg)

        if in_string:
            args.append(' '.join(string_buffer))

        return args


    async def parse_args_v2(self, ctx, msg, command):
        try:
            args = command.arg_parser.parse_args(msg)
            if args.h:
                await ctx.say('```\n' + command.arg_parser.format_usage() + '```')
                return False
            else:
                return args
        except ArgException as e:
            if 'on_invalid_args' in self.events:
                await self.events['on_invalid_args'](ctx)
            else:
                await ctx.say(
                    '```\n' +
                    command.arg_parser.format_help() +
                    '```\n```\n' +
                    e.msg + '```'
                )

            return False


    async def process_command(self, message):

        is_command, content, matched_prefix = self.match_prefix(message)
        # for prefix in self.command_prefixes:
        #
        #     check_result = prefix.check(message.content)
        #     if (check_result[0]):
        #         is_command = True
        #         matched_prefix = prefix
        #         break


        if is_command:

            content = content.strip().split()

            command_name = content[0]
            if command_name not in self.commands:
                if command_name in self.aliases:
                    command_name = self.aliases[command_name]
                else:
                    raise KeyError('No command found: "{}"'.format(command_name))

            command = self.commands[command_name]

            raw_args = content[1:]

            context = Context(self.client, self, message, command, self.context_hook)
            await context.run_hook()

            if command.use_advanced_args:
                args = await self.parse_args_v2(context, raw_args, command)
                if args == False: return

                arg_list = []
                kwarg = {'args': args}
            else:
                args_list = self.parse_args_v1(context, raw_args)
                kwarg = {}

            try:
                command.is_allowed(context, self.global_checks)
            except NsfwError as e:
                if 'on_nsfw_error' in self.events:
                    await self.events['on_nsfw_error'](context)
                return
            except CheckError as e:
                if self.auto_send_check_errors:
                    await context.say(e.check.err(context))

                if 'on_check_failure' in self.events:
                    await self.events['on_check_failure'](context, e.check)
                return
            except UserPermissionError as e:
                if 'on_missing_permission' in self.events:
                    await self.events['on_missing_permission'](context, e.permission)
                return
            except ClientPermissionError as e:
                if 'on_missing_bot_permission' in self.events:
                    await self.events['on_missing_bot_permission'](context, e.permission)
                return

            try:
                if command.send_typing:
                    await context.channel.trigger_typing()

                if command.pass_context:
                    await command.function(context, *args_list, **kwarg)
                else:
                    await command.function(*args_list, **kwarg)
                try:
                    if 'on_command_success' in self.events:
                        await self.events['on_command_success'](context, command)
                except:
                    pass # Need to actually handle error here

            except Exception as e:
                if 'on_command_error' in self.events:
                    tb = traceback.format_exc()
                    await self.events['on_command_error'](context, e, tb)
                else:
                    raise e



    def command(self, *args, **kwargs):
        """
        Decorator to declare commands.
        """
        def decorator(function):
            cmd = Command(function, *args, **kwargs)

            self.commands[cmd.name] = cmd
            for alias in cmd.aliases:
                self.aliases[alias] = cmd.name

            return cmd

        return decorator


    def event(self, func):
        """
        Decorator to declare handler events, possible are
            -on_command_success
            -on_command_error
            -on_missing_permission
            -on_check_failure
        All events are called with (ctx, command name, and class name)
        """
        self.events[func.__name__] = func

    def generate_help(self, variant=0, *, ordering={}, command_ordering={},
                      command_sort_key=lambda x: x.name):
        string = ''
        cogs   = collections.OrderedDict()

        for command in self.commands:
            command = self.commands[command]
            if command.cmd_group not in cogs:
                cogs[command.cmd_group] = []
            cogs[command.cmd_group].append(command)

            if command.name not in command_ordering:
                command_ordering[command.name] = 0



        for cog in cogs:
            if cog not in ordering:
                ordering[cog] = 0

            if command_sort_key is not None:
                cogs[cog] = sorted(cogs[cog], key=lambda x: (command_ordering[x.name], command_sort_key(x)))

        sorted_cogs = sorted(cogs.items(), key=lambda x: (ordering[x[0]], x[0]) )
        cogs        = collections.OrderedDict(sorted_cogs)
        cog_list    = []

        # cogs.move_to_end('Main', last=False)

        def format_command(command):
            args = command.pretty_args()

            name = command.name
            if command.aliases != []:
                name += ' ({})'.format(', '.join([a for a in command.aliases]))

            if command.description in ['', None]:
                desc = ''
            else:
                desc = ': ' + command.description

            if args == []:
                return '{0}{1}'.format(name, desc)
            else:
                return '{0} {1}{2}'.format(name, ' '.join(args), desc)

        if variant == 0:
            for cog in cogs:
                extension_string = '**{}**:\n```\n'.format(cog)
                display = False
                for command in cogs[cog]:
                    if not command.hide_in_help:
                        extension_string += format_command(command) + '\n'
                        display = True

                if display:
                    string += extension_string + '```\n'

        elif variant == 1:
            for cog in cogs:
                string += '{}\n```'
                string += ', '.join(cog)
                string += '```\n\n'

        return string

def command(*args, **kwargs):
    """
    Command decorator for extensions
    """
    def wrapper(func):
        return Command(func, *args, **kwargs)

    return wrapper

def add_arg(name, *, desc=None, meta_name=None, default=None, n=1,
            required=False, choices=None, type=None):
    """ Command decorator for add arg_parser arguments
    """
    meta_name = meta_name or name

    def f(cmd):
        cmd.arg_parser.add_argument(
            name,
            nargs=n,
            help=desc,
            type=type,
            choices=choices,
            default=default,
            metavar=meta_name,
        )
        return cmd

    return f

def raw_add_arg(cmd, *args, **kwargs):
    """ Directly calls `arg_parser.add_argument` with the provided
        args and kwargs
    """

    cmd.arg_parser.add_argument(*args, **kwargs)
    return cmd
