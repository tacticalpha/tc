import argparse
import inspect

from tc.exceptions import *

from tc.classes import Check
from tc.classes.ThrowingArgumentParser import ThrowingArgumentParser

class Command:

    def __init__(
        self, function, *extra_args, description='', pass_context=True, name_overwrite=None,
        send_typing=True, checks=[], aliases=[], groups=[], cmd_group=None, nsfw=False,
        required_permissions=[], required_server_perms=[], hide_in_help=False,
        cooldowns=[], permission_mode=0, use_advanced_args=False, parser=None,
        advanced_args=[], **extra_kwargs
    ):
        self.name                  = function.__name__ if name_overwrite is None else name_overwrite
        self.function              = function
        self.description           = description
        self.cmd_group             = cmd_group
        self.pass_context          = pass_context
        self.checks                = checks
        self.aliases               = aliases
        self.send_typing           = send_typing
        self.groups                = groups
        self.nsfw                  = nsfw
        self.required_permissions  = required_permissions
        self.required_server_perms = required_server_perms
        self.hide_in_help          = hide_in_help
        self.cooldowns             = cooldowns
        self.advanced_args         = advanced_args
        self.use_advanced_args     = use_advanced_args

        if use_advanced_args:
            self.arg_parser = parser or ThrowingArgumentParser(
                prog=self.name, description=self.description, add_help=False
            )
            self.arg_parser.add_argument('-h')
            # self.arg_parser.add_argument('__args__' , nargs='*')

            for arg in advanced_args:
                arg.add_to(self.arg_parser)

        # Arbitrary arguments and kw arguments allows for the
        # user to further extend command functionality without
        # reqiuring a modification to the source.
        self.extra_args           = extra_args
        self.extra_kwargs         = extra_kwargs

        i = 0
        for check in self.checks:
            if not isinstance(check, Check):
                self.checks.pop(i)
                self.checks.insert(i, Check(check))

            i += 1

    def is_allowed(self, ctx, global_checks):

        if ctx.handler.ignore_checks_for_admins:
            if ctx.author.id in ctx.handler.admins:
                return

        channel_permissions = ctx.channel.permissions_for(ctx.author)
        permissions = ctx.author.guild_permissions

        client_channel_permissions = ctx.channel.permissions_for(ctx.client_member)
        client_permissions = ctx.client_member.guild_permissions

        for check in global_checks:
            if not check(ctx):
                raise CheckError(check)

        for check in self.checks:
            if not check(ctx):
                raise CheckError(check)

        if self.nsfw:
            if not ctx.channel.is_nsfw():
                raise NsfwError()

        for perm in self.required_permissions:
            if not getattr(channel_permissions, perm):
                if not getattr(permissions, perm):
                    raise UserPermissionError(perm)

        for perm in self.required_server_perms:
            if not getattr(client_channel_permissions, perm):
                if not getattr(client_permissions, perm):
                    raise ClientPermissionError(perm)


    def add_arg(self, args, kwargs):
        self.arg_parser.add_argument(*args, **kwargs)


    def pretty_args(self, *, required_chars='<>', optional_chars='[]'):
        #
        params = list(map(lambda x: x, inspect.signature(self.function).parameters.values()))
        args = []
        for param in params[1 if self.pass_context else 0:]:
            kind = param.kind.name
            if kind == 'POSITIONAL_ONLY' or (kind == 'POSITIONAL_OR_KEYWORD' and param.default in ['', None, inspect._empty]):
                args.append('{1[0]}{0.name}{1[1]}'.format(param, required_chars))
            elif kind == 'POSITIONAL_OR_KEYWORD':
                args.append('{1[0]}{0.name} = {0._default}{1[1]}'.format(param, optional_chars))

        return args
