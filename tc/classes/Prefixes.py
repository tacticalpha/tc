import re


class Prefix:
    def __init__(self, type, value):
        self.type  = type
        self.value = value
        # raise TypeError('Prefix has an invalid type: "{}"'.format(type))

    def __str__(self):
        return "<tc.Handler.Prefix, type={}, value=\"{}\">".format(self.type, self.value)


class ExactPrefix(Prefix):

    def __init__(self, text):
        super().__init__('exact', text)

    def check(self, text):
        if text.startswith(self.value):
            return (True, text[len(self.value):])
        else:
            return (False, '')


class RegexPrefix(Prefix):

    def __init__(self, re_pattern):
        super().__init__('regex', re.compile(re_pattern))

    def check(self, text):
        match = self.value.match(text)
        if match is not None:
            return (True, match.group(1))
        else:
            return (False, '')


class MentionPrefix(RegexPrefix):

    def __init__(self, id):
        super().__init__('^<@!?{0}>(.*)'.format(id))
        self.type = 'mention'
