
class Arg:
    def __init__(self, name, desc=None, meta_name=None,
                 default=None, n=1, required=False,
                 choices=None, type=None):

        self.name      = name
        self.meta_name = meta_name or name
        self.desc      = desc
        self.default   = default
        self.n         = n
        self.required  = required,
        self.choices   = choices
        self.type      = type

    def args(self):
        return [self.name]

    def kwargs(self):
        return {
            'nargs': self.n,
            'help': self.desc,
            'type': self.type,
            'choices': self.choices,
            'default': self.default,
            'metavar': self.meta_name,
        }

    def add_to(self, parser):
        parser.add_argument(
            self.name,
            nargs   = self.n,
            help    = self.choices,
            type    = self.type,
            choices = self.choices,
            default = self.default,
            metavar = self.meta_name,
        )
