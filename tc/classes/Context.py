import asyncio

class Context:

    def __init__(
        self,
        client,
        handler,
        message,
        command,
        hook,
        *,
        author  = None,
        guild   = None,
        channel = None,
        voice   = None
    ):

        self.client  = client
        self.handler = handler
        self.command = command

        self.message = message
        if message is not None:
            self.author  = message.author
            self.guild   = message.guild
            self.channel = message.channel
            self.voice   = message.author.voice
        else:
            self.author  = author
            self.guild   = guild
            self.channel = channel
            self.voice   = voice

        self.client_member = self.guild.get_member(client.user.id) if self.guild is not None else None

        self.hook = hook

    def __str__(self):

        return "<tc.Handler.Context command='{}' author='{}' guild='{}' channel='{}' message='{}'>".format(
            self.command.name,
            self.author.name + '#' + self.author.discriminator,
            self.guild.name,
            self.channel.name,
            self.message.content[0:47] + '...' if len(self.message.content) > 50 else self.message.content
        )


    async def run_hook(self):
        if (asyncio.iscoroutinefunction(self.hook)):
            await self.hook(self)
        else:
            self.hook(self)


    async def _send(self, to, message_content=None, *, tts=False, embed=None, files=[], delete_after=None):

        number_of_files = len(files)
        if number_of_files > 0:
            return await to.send(
                message_content,
                tts=tts,
                embed=embed,
                delete_after=delete_after,
                **({'files': files} if len(files) > 1 else {'file': files[0]}) # same as files=files if len > 1 or file=files[0] otherwise
            )

            # These shouldn't be needed anymore if the above code works
            # ---
            # if number_of_files == 1:
            #     return await to.send(message_content, tts=tts, embed=embed, file=files[0], delete_after=delete_after)
            # elif number_of_files > 1:
            #     return await to.send(message_content, tts=tts, embed=embed, files=files, delete_after=delete_after)
        else:
            return await to.send(message_content, tts=tts, embed=embed, delete_after=delete_after)


    async def say(self, message_content=None, **kwargs):

        return await self._send(self.channel, message_content, **kwargs)

    async def dm_author(self, message_content=None, **kwargs):

        return await self._send(self.author, message_content, **kwargs)
