import argparse

from tc.exceptions import ArgException

class ThrowingArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        raise ArgException(message)
