## What is this
---
I used to use discord.commands or discord.extension.commands or whatever but I needed to do a thing that was hard with that so I made my own. Honestly I don't remember the reason but I've been using this since.

This is a replacement for that command handler, it supports most of the features that the default one has _(that I use)_ and has many more _(buuuut they're not documented (at least not now) so good luck)_.

I don't really expect anyone to use this except for maybe my friends so yeah

## Installation
---
```python3 -m install git+https://gitlab.com/tacticalpha/tc```
