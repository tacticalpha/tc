"""
My command handler for discordpy. Pretty self explanitory.
"""

from setuptools import setup, find_packages

from os import path

import sys

setup_dir = path.abspath(path.dirname(__file__))

with open(path.join(setup_dir, 'readme.md')) as f:
    long_desc = f.read()

setup(
    name="tactical-command-handler",

    version='1.0.0',

    description='A command handler for discordpy',

    long_description=long_desc,

    url='https://gitlab.com/TacticAlpha/tc',

    author='Tactic Alpha',

    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: AGPL-3.0',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],

    keywords='python tools utilities utility class classes',

    packages=find_packages(),

    entry_points={},
    options={}
)
